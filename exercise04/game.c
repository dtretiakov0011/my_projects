#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "my_types.h"
#include "my_list.h"

static void print_menu(void);
static DbLinkedList * read_players_name(int player_count);
static void play_game(DbLinkedList *players_list);
static void throw_rand(int power, player_t *p_player);

static DbLinkedList *read_players_name(int player_count) {
    DbLinkedList *players_list = createDbLinkedList();
        if(players_list == NULL)
            exit(1);

    int i;
    for(i = 1; i <= player_count; i++) {
        char buff[50] = { 0 };
        char *new_name;

        printf("Enter %d player name: ", i);
        scanf("%s", buff);
        new_name = strdup(buff);
        pushFront(players_list, new_name);
    }
    return players_list;
}

static void print_menu(void) {
    int player_count = 0;

    printf("How many players?(min 1 max 5) : ");
    scanf("%d", &player_count);
    if(player_count > 5 || player_count < 1)
        return;

    DbLinkedList *players_list = read_players_name(player_count);
    play_game(players_list);
    deleteDbLinkedList(&players_list);
}

static void print_game_m1(void) {
    printf("********************-Starting The Game-*\n");
    int i;
    for(i = 0; i < 1; i++) {
        printf("****************************************\n");
        sleep(1);
    }
}

static void print_game_m2(DbLinkedList *players_list) {
    int i = 0, power = 0;
    player_t *p_player = (player_t*)players_list->head;

    for(; i < players_list->size; ++i) {
        p_player = container_of(p_player, player_t, player_node);
        printf("  Player %s enter your power for throw: ", p_player->name);
    //must check power
        scanf("%d", &power);
        throw_rand(power, p_player);

        p_player = (player_t*)p_player->player_node.next;
    }
    printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n");

    p_player = (player_t*)players_list->head;
   
    for(i = 0; i < players_list->size; ++i) {
        p_player = container_of(p_player, player_t, player_node);
        printf("  Player %s score: %d\n", p_player->name, p_player->score);
        p_player = (player_t*)p_player->player_node.next;
    }
}

static void throw_rand(int power, player_t *p_player) {
    printf("\n");
    srand(time(NULL) + power);
    int score = 1 + rand()%(6 - 1 + 1);
    printf("\t%s score: %d\n", p_player->name, score);
    p_player->score += score;
}

static void play_game(DbLinkedList *players_list) {
    print_game_m1();
    print_game_m2(players_list);
}



void menu(void) {
    print_menu();
}


