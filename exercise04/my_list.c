#include <stdlib.h>
#include "my_types.h"
#include "my_list.h"

DbLinkedList *createDbLinkedList(void) {
    DbLinkedList *new_list = (DbLinkedList*)malloc(sizeof(DbLinkedList));
    new_list->size = 0;
    new_list->head = new_list->tail = NULL;
    
    return new_list;
}

void deleteDbLinkedList(DbLinkedList **list) {
    node_t *tmp = (*list)->head;
    node_t *next = NULL;

    while(tmp) {
        next = tmp->next;
        player_t *p_player = container_of(tmp, player_t, player_node);      
        free(p_player->name);
        tmp = next;
    }
    free(*list);
    (*list) = NULL;
}

void pushFront(DbLinkedList *list, void *data) {
    player_t *new_player = (player_t*)malloc(sizeof(player_t));  
        if(new_player == NULL)
            exit(1);

    new_player->name = (char*)data;
    new_player->score = 0;
    new_player->player_node.next = list->head;
    new_player->player_node.prev = NULL;

    if(list->head) {
        list->head->prev = &new_player->player_node;
    }
    list->head = &new_player->player_node;

    if(list->tail == NULL) {
        list->tail = &new_player->player_node;
    }
    list->size++;
}

