#ifndef _MY_LIST_H_
#define _MY_LIST_H_

#include <stdio.h>
#include <_types.h>
#include <stddef.h>

typedef struct node {
    struct node *next;
    struct node *prev;
} node_t;

typedef struct _DbLinkedList {
    size_t size;
    node_t *head;
    node_t *tail;
} DbLinkedList;

#define container_of(ptr, type, member) ((type *)((char *)(ptr) - offsetof(type, member)))

DbLinkedList *createDbLinkedList(void);
void deleteDbLinkedList(DbLinkedList **list);
void pushFront(DbLinkedList *list, void *data);

#endif
