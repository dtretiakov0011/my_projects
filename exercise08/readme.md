#1     
    git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-b v5.11 --depth 1
    cd linux-stable
    export BUILD_KERNEL=<your_build_path>
    make O=${BUILD_KERNEL} i386_defconfig  ///// make ARCH=i386 O=${BUILD_KERNEL} defconfig
    cd ${BUILD_KERNEL}
    make menuconfig
    make (you can use -j4 to speedup building)

#2
    git clone git://git.buildroot.net/buildroot
    cd buildroot
    export BUILD_ROOTFS=<your_build_path>
    make O=${BUILD_ROOTFS} qemu_x86_defconfig
    cd ${BUILD_ROOTFS}
    make menuconfig

    ○ Create user record
        echo "user 1000 user 1000 =pass /home/user /bin/bash - LinuxUser" > ${BUILD_ROOTFS}/users
            (This will create user user with id 1000 and password pass)
    ○ Add the user to sudoers
        mkdir -p ${BUILD_ROOTFS}/root/etc/sudoers.d
        echo "user ALL=(ALL) ALL" > ${BUILD_ROOTFS}/root/etc/sudoers.d/user
    ○ Create list of shells for dropbear
        mkdir -p ${BUILD_ROOTFS}/root/etc
        echo "/bin/sh" > ${BUILD_ROOTFS}/root/etc/shells
        echo "/bin/bash" >> ${BUILD_ROOTFS}/root/etc/shells
● Build the FS
    make   
        (It will take some time, but eventually you'll get ${BUILD_ROOTFS}/images/rootfs.ext3)
################################################################################################
Launch QUEMU (for last -v)
qemu-system-i386 -kernel ${BUILD_KERNEL}/arch/i386/boot/bzImage -append "root=/dev/sda console=ttyS0" -drive format=raw,file=${BUILD_ROOTFS}/images/rootfs.ext3 -nic user,hostfwd=tcp::8022-:22 &

