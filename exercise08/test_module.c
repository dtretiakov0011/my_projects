#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("admin");
MODULE_DESCRIPTION("A sinmple test module");
MODULE_VERSION("0.01");

static int __init test_m_init(void)
{
    printk(KERN_INFO "Hello\n");

    return 0;
}

static void __exit test_m_exit(void)
{
    printk(KERN_INFO "Bye...\n");
}

module_init(test_m_init);
module_exit(test_m_exit);
