#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <alloca.h>



int main(void) {


    int size_block = 8;

    int * malloc_p;
    int * calloc_p;
    int * alloca_p;

    clock_t malloc_start;
    clock_t malloc_end;

    clock_t calloc_start;
    clock_t calloc_end;

    clock_t alloca_start;
    clock_t alloca_end;

    while(1)
    {
        malloc_start = clock();
        malloc_p = malloc(size_block);
        if( malloc_p != NULL )
        {    
            malloc_p[5] = 5;
            malloc_end = clock();
            free(malloc_p);
        }
        
        calloc_start = clock();
        calloc_p = calloc(size_block, 1);
        if( calloc_p != NULL )
        {
            calloc_p[5] = 5;
            calloc_end = clock();
            free(calloc_p);
        }

        alloca_start = clock();
        alloca_p = alloca(size_block);
        alloca_p[0] = 5;
        alloca_end = clock();




        printf("malloc size %d time %f\n", size_block, (double)(malloc_end - malloc_start) / CLOCKS_PER_SEC);
        printf("calloc size %d time %f\n", size_block, (double)(calloc_end - calloc_start) / CLOCKS_PER_SEC);
        printf("alloca size %d time %f\n", size_block, (double)(alloca_end - alloca_start) / CLOCKS_PER_SEC);
        printf("++++++++++++++++++++++++\n");
        
        size_block *= 2;
    }



}