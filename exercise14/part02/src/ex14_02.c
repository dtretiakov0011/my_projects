#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/jiffies.h>
#include <linux/types.h>
#include<linux/slab.h>
//include/linux/vmalloc.h
#include <linux/vmalloc.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("admin");
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.01");

u64 i;



static int __init ex01_init(void)
{
    i = get_jiffies_64();

    int size_alloc[5] = { 1, 500, 2000, 50000, 500000};

    void * p_to_memory = NULL;

    u64 kmalloc_time, kzalloc_time, vmalloc_time, get_free_pages_time;

    int y, order;
    for( y = 0, order = 0; y < 5; ++y, ++order )
    {
        i = get_jiffies_64();
        p_to_memory = kmalloc( size_alloc[y], GFP_KERNEL );
        if( p_to_memory != NULL )
        {
            *(char*)p_to_memory = '7';
            kfree(p_to_memory);

            kmalloc_time = get_jiffies_64();
            kmalloc_time -= i;
            kmalloc_time = jiffies64_to_nsecs(kmalloc_time);
            printk( "size =%d kmalloc time =%lld", size_alloc[y], kmalloc_time );
        } 

        i = get_jiffies_64();
        p_to_memory = kzalloc( size_alloc[y], GFP_KERNEL );
        if( p_to_memory != NULL )
        {
            *(char*)p_to_memory = '7';
            kfree(p_to_memory);

            kzalloc_time = get_jiffies_64();
            kzalloc_time -= i;
            kzalloc_time = jiffies64_to_nsecs(kzalloc_time);
            printk( "size =%d kzalloc time =%lld", size_alloc[y], kzalloc_time );
        }

        i = get_jiffies_64();
        p_to_memory = vmalloc( size_alloc[y] );
        if( p_to_memory != NULL )
        {
            *(char*)p_to_memory = '7';
            vfree(p_to_memory);

            vmalloc_time = get_jiffies_64();
            vmalloc_time -= i;
            vmalloc_time = jiffies64_to_nsecs(vmalloc_time);
            printk( "size =%d vmalloc time =%lld", size_alloc[y], vmalloc_time );
        }

        i = get_jiffies_64();
        p_to_memory = (char *)__get_free_pages( GFP_KERNEL, order );
        if( p_to_memory != NULL )
        {
            *(char*)p_to_memory = '7';
            free_pages((unsigned long)p_to_memory, order);

            get_free_pages_time = get_jiffies_64();
            get_free_pages_time -= i;
            get_free_pages_time = jiffies64_to_nsecs(vmalloc_time);
            printk( "size =%d __get_free_pages time =%lld", order, get_free_pages_time );
        }

        printk( "+++++++++++++++++++++\n" );
    }




    printk(KERN_INFO "Hello!!!\n");
    return 0;
}




static void __exit ex01_exit(void)
{
//     u64 i_exit = get_jiffies_64();
//     i_exit -= i;

//     u64 res = jiffies64_to_nsecs(i_exit);

//  printk( "finish=%lld|\n", res );

    printk(KERN_INFO "Bye...\n");
}

module_init(ex01_init);
module_exit(ex01_exit);
