#include <linux/init.h>
#include <linux/module.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>

static struct gpio en;
static int irq_num;
static struct gpio en2;
static int irq_num2;
int my_counter = 0;


static irqreturn_t gpio_isr(int irq, void *data)
{
	*(int*)data += 1;
	printk(KERN_INFO "encoder: switch pressed  %d ***%d\n", irq, *(int*)data);

	return IRQ_HANDLED;
}

static irqreturn_t my_gpio_isr(int irq, void *data)
{
	printk(KERN_INFO "encoder: switch pressed  %d ***%d\n", irq, *(int*)data);

	return IRQ_HANDLED;
}

static void my_gpio(void)
{
	int ret;

	en2.gpio = 27;
	en2.flags = GPIOF_IN;
	en2.label = "In Gpio encoder2";

	ret = gpio_request(en2.gpio, en2.label);
	if (ret) {
		printk(KERN_ERR "encoder: failed to request GPIO: %d\n", ret);
        	return ;
	}

	ret = gpio_to_irq(en2.gpio);
	if (ret < 0) {
		printk(KERN_ERR "encoder: failed to request IRQ: %d\n", ret);
        	return ;
	}

	irq_num2 = ret;
    	printk(KERN_INFO "encoder: requested IRQ#%d.\n", irq_num2);
    	ret = request_irq(irq_num2, my_gpio_isr, IRQF_TRIGGER_RISING  | IRQF_TRIGGER_FALLING | UMH_DISABLED, "gpio_irq", (void*)&my_counter);
	if (ret) {
		printk(KERN_ERR "encoder: failed to request IRQ\n");
        	return ;
	}

}

static int __init encoder_init(void)
{
	int ret;

	en.gpio = 22;
	en.flags = GPIOF_IN;
	en.label = "In Gpio encoder";

	ret = gpio_request(en.gpio, en.label);
	if (ret) {
		printk(KERN_ERR "encoder: failed to request GPIO: %d\n", ret);
        	return -1;
	}

	ret = gpio_to_irq(en.gpio);
	if (ret < 0) {
		printk(KERN_ERR "encoder: failed to request IRQ: %d\n", ret);
        	return -2;
	}

	irq_num = ret;
    	printk(KERN_INFO "encoder: requested IRQ#%d.\n", irq_num);
    	ret = request_irq(irq_num, gpio_isr, IRQF_TRIGGER_RISING  | IRQF_TRIGGER_FALLING | UMH_DISABLED, "gpio_irq", (void*)&my_counter);
	if (ret) {
		printk(KERN_ERR "encoder: failed to request IRQ\n");
        	return -3;
	}

	my_gpio();

	printk(KERN_INFO "encoder: module loaded\n");
	return 0;
}

static void __exit encoder_exit(void)
{
	free_irq(irq_num,NULL);
	free_irq(irq_num2,NULL);
	gpio_free(en.gpio);
	gpio_free(en2.gpio);
	printk(KERN_INFO "encoder: module exited\n");
}

module_init(encoder_init);
module_exit(encoder_exit);

MODULE_AUTHOR("Maksym.Lipchanskyi <maxl@meta.ua>");
MODULE_DESCRIPTION("Encoder Demo");
MODULE_INFO(intree, "Y");
MODULE_LICENSE("GPL");
