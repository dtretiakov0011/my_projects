#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#define POOL_SIZE 2

typedef struct ex16
{
    char        c;
    unsigned    blocks_c;
    unsigned    len;
}              ex16_s;

unsigned counter_s = 0;

ex16_s my_s[10];


void * write_block_len_characters( void * data );
void open_conf_file(void);
void add_to_struct( char *buf );



int main(void)
{
    open_conf_file();

    int arg_c = 0;    
    pthread_t threads[ POOL_SIZE ];

    for( int i = 0; i < POOL_SIZE; ++i )
    {
        if( arg_c < counter_s )
        {
            threads[i] = pthread_create( &threads[i], NULL, write_block_len_characters, (void *)(my_s + arg_c) );
            arg_c++;
        }
    }

    pthread_join( threads[0], NULL );
    pthread_join( threads[1], NULL );



}


void * write_block_len_characters( void * data )
{
    ex16_s * p_s = (ex16_s*)data;

    char c = p_s->c; unsigned blocks = p_s->blocks_c; unsigned len = p_s->len;
    int count;

    printf("%c %d %d\n", c, blocks, len);
    //write( STROU );
    write( STDIN_FILENO, "2", 1 );
    for( ; blocks != 0; --blocks )
    {
        for( count = len; count != 0; --count )
        {
            write( STDIN_FILENO, &c, 1 );
            sleep(1);
        }
        write( STDIN_FILENO, "\n", 1 );
        sleep(1);
    }
    return data;
}

void open_conf_file(void)
{
    char buff[10] = { 0 };
    int fd = open( "conf.txt", O_RDONLY ), i = 0, ret;

    do
    {
        ret = read( fd, &buff[i], 1 );
        
        if( buff[i] == '\n' )
        {
            //obrabotka
            add_to_struct( buff );
            i = 0;
        }
        else
            i++;
    } while ( ret != 0 );
}

void add_to_struct( char *buf )
{
    my_s[counter_s].c = *buf;
    my_s[counter_s].blocks_c = *(buf + 1) - '0';
    my_s[counter_s].len = *(buf + 2) - '0';
    
    counter_s++;
}
