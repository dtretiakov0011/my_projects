#ifndef _BUTTONS_H_
#define _BUTTONS_H_

//#include <fcntl.h>

typedef enum condition
{
    RIGHT,
    LEFT,
    MENU,
    UNKNOWN
}           condition_e;

#define GPIO_PIN_KEY1 18
#define GPIO_PIN_KEY2 23
#define GPIO_PIN_KEY3 24

#define GPIO_PIN_KEY1_DIR "/sys/class/gpio/gpio18/value"
#define GPIO_PIN_KEY2_DIR "/sys/class/gpio/gpio23/value"
#define GPIO_PIN_KEY3_DIR "/sys/class/gpio/gpio24/value"


void lcd_button_init( void );
void lcd_button_free( void );

void read_check_all_buttons( void );
uint8_t lcd_button_read( int fd );

void open_fd_arr( int * fd_arr );
void close_fd_arr( int * fd_arr );


#endif
