#include "ili9341.h"
#include "snake.h"


snake_s mySnake;
bug_s BUG;
condition_e con_e;
int EROR_INFO;



/***************************************************************************/


//Y_max = 230
//X_max = 310
//Y_start = 20
//X_start = 0

void Test_LSD(void)
{
	new_game();

	play_game(&mySnake);

	lcd_fill_rectangle( 110, 80, 100, 100, COLOR_S_YELLOW);
    lcd_put_str(110, 120, "GAME OVER", Font_11x18, COLOR_RED, COLOR_BLACK);
	lcd_update_screen();
}



void play_game(snake_s * Snake)
{
    coor_s * coor_head = NULL;
	coor_s * coor_tail = NULL;

	while(1)
	{	
		coor_head = container_of_my( (coor_s*)Snake->cells->head, coor_s, coor_node );
		coor_tail = container_of_my( (coor_s*)Snake->cells->tail, coor_s, coor_node );
		
		direction_trigger( coor_head, coor_tail );
        if(EROR_INFO == 1)
            return;
		if( Snake->del_tail )
			snake_clean_tail( coor_tail );
		else
			Snake->del_tail = 1;
		
		lcd_update_screen();
	}
}









void direction_trigger( coor_s *head, coor_s *tail )
{
	read_check_all_buttons();

	//trigers!!!!
	switch ( con_e )
	{
		case UNKNOWN:
			snake_move_forward( head, tail );
			break;
		case LEFT:
			snake_move_left( head, tail );
			break;
		case RIGHT:
			snake_move_right( head, tail );
			break;
		default:
			break;
	}
	con_e = UNKNOWN;
}









/*
	which coordinate does not change
	0 if x does not change
	1 if y does not change
*/
int check_priv_coor_XY( coor_s * head, coor_s * prev )
{
	if( head->x == prev->x )
		return 0;
	else
		return 1;
}


void check_new_coor( coor_s * new_coor )
{
	if( new_coor->x > 310 || new_coor->y < 20 || new_coor->y > 230 || check_new_coor_with_snake(new_coor) )
	{
        END_GAME();
        EROR_INFO = 1;
        return;
	}

	pushFront( mySnake.cells, new_coor );
	lcd_fill_rectangle( new_coor->x, new_coor->y, SNAKE_W, SNAKE_H, mySnake.color);

	if( new_coor->x == BUG.xy.x && new_coor->y == BUG.xy.y )
	{
		mySnake.del_tail = 0;
		mySnake.score++;
        print_score();  
		bug_rand();
		bug_print();
	}
}

int check_new_coor_with_snake( coor_s * new_coor )
{
    int i;
    node_t * p_node = mySnake.cells->head;
    coor_s * p_coor;

    for( i = 0; i < mySnake.cells->size; i++ )
    {
        p_coor = (coor_s*)p_node;
        p_coor = container_of_my(p_coor, coor_s, coor_node);

        if( p_coor->x == new_coor->x && p_coor->y == new_coor->y )
             return 1;
 
        p_node = p_node->next;
    }
    return 0;
}



void snake_move_left( coor_s * head, coor_s * tail )
{
	coor_s * xy_prev = container_of_my( (coor_s*)head->coor_node.next, coor_s, coor_node );
	int check = check_priv_coor_XY( head, xy_prev );
	coor_s * new_coor = NULL;

	if( check )	// work with Y
	{
		if( (xy_prev->x) < (head->x) )
			new_coor = new_cell_init( head->x, head->y - 10 );
		else
			new_coor = new_cell_init( head->x, head->y + 10 );
	}
	else	// work with X
	{
		if( (xy_prev->y) > (head->y) )
			new_coor = new_cell_init( head->x - 10, head->y );
		else
			new_coor = new_cell_init( head->x + 10, head->y );
	}
	//check new cell add & print new head
	check_new_coor( new_coor );
}

void snake_move_right( coor_s * head, coor_s * tail )
{
	coor_s * xy_prev = container_of_my( (coor_s*)head->coor_node.next, coor_s, coor_node );
	int check = check_priv_coor_XY( head, xy_prev );
	coor_s * new_coor = NULL;

	if( check )	// work with Y
	{
		if( (xy_prev->x) > (head->x) )
			new_coor = new_cell_init( head->x, head->y - 10 );
		else
			new_coor = new_cell_init( head->x, head->y + 10 );
	}
	else	//work with X
	{
		if( (xy_prev->y) < (head->y) )
			new_coor = new_cell_init( head->x - 10, head->y );
		else
			new_coor = new_cell_init( head->x + 10, head->y );
	}
	//check new cell add & print new head
	check_new_coor( new_coor );
}

void snake_move_forward( coor_s * head, coor_s * tail )
{
	coor_s * xy_prev = container_of_my( (coor_s*)head->coor_node.next, coor_s, coor_node );
	int check = check_priv_coor_XY( head, xy_prev );
	coor_s * new_coor = NULL;

	if( !check ) //work with Y
	{
		if( (xy_prev->y) < (head->y) )
			new_coor = new_cell_init( head->x, head->y + 10 );
		else
			new_coor = new_cell_init( head->x, head->y - 10 );
	}
	else		//work with X
	{
		if( (xy_prev->x) < (head->x) )
			new_coor = new_cell_init( head->x + 10, head->y );
		else
			new_coor = new_cell_init( head->x - 10, head->y );
	}
	//check new cell add & print new head
	check_new_coor( new_coor );
}



void END_GAME( void ) 
{
    deleteLinkedList( &mySnake.cells );
    lcd_button_free();
}



void snake_clean_tail( coor_s * tail )
{
	lcd_fill_rectangle( tail->x, tail->y, SNAKE_W, SNAKE_H, COLOR_BLUE);
	deleteTail(mySnake.cells);
}


void check_buttons( uint8_t * keys )
{
	//MENU
	if( keys[2] == 0 )
		EROR_INFO = 1;//menu | exit
	if( keys[1] == 0 )
		con_e = RIGHT;
	else if( keys[0] == 0 )
		con_e = LEFT;
}


void read_check_all_buttons( void )
{
	uint8_t res_keys[3];
    
	res_keys[0] = gpio_get_value(GPIO_PIN_KEY1);
	res_keys[1] = gpio_get_value(GPIO_PIN_KEY2);
	res_keys[2] = gpio_get_value(GPIO_PIN_KEY3);

	check_buttons( res_keys );

}




void lcd_button_init(void) 
{
    gpio_request(GPIO_PIN_KEY1, "GPIO_PIN_KEY1");
    gpio_request(GPIO_PIN_KEY2, "GPIO_PIN_KEY2");
    gpio_request(GPIO_PIN_KEY3, "GPIO_PIN_KEY3");
    gpio_direction_output(GPIO_PIN_KEY1, 1);
    gpio_direction_output(GPIO_PIN_KEY2, 1);
    gpio_direction_output(GPIO_PIN_KEY3, 1);
}

void lcd_button_free(void) 
{
    gpio_free(GPIO_PIN_KEY1);
    gpio_free(GPIO_PIN_KEY2);
    gpio_free(GPIO_PIN_KEY3);
}





void new_game(void)
{
	lcd_fill_screen(COLOR_S_BLUE);
	//display
	lcd_fill_rectangle( 0, 0, LCD_WIDTH, 20, COLOR_S_BLACK );
	//snake_start
	snake_Init( &mySnake );
	snake_print_all( &mySnake );

    lcd_button_init();

	//BUG_start
	bug_rand();
	bug_print();

	con_e = UNKNOWN;
    EROR_INFO = 0;
    lcd_put_str(0, 0, "score:", Font_11x18, COLOR_RED, COLOR_BLACK);
    print_score();

	lcd_update_screen();
    msleep(2000); // 2 sec
}



/*
*
*
*
SNAKE FUNCTION
*
*
*
*/

void snake_Init( snake_s * Snake )
{
    Snake->cells = createLinkedList();
    Snake->color = COLOR_S_GREEN;
    Snake->score = 0;
    Snake->del_tail = 1;

    cells_start_init( Snake->cells );
    Snake->size = 5;
}

void coor_xy_zero( coor_s * coor_xy, int size )
{
    int i; 
    for(i = 0; i < size; ++i )
    {
        coor_xy[i].x = __INT_LEAST16_MAX__;
        coor_xy[i].y = __INT_LEAST16_MAX__;
    }
}

void print_score( void )
{
    char buf[24] = { 0 };

    sprintf( buf, "%d", mySnake.score );
    //lcd_put_char(0, 0, 'A', Font_11x18, COLOR_RED, COLOR_BLACK);
    lcd_put_str(70, 0, buf, Font_11x18, COLOR_RED, COLOR_BLACK);
}

void cells_start_init( LinkedList * cells )
{
    coor_s * cell_0 = new_cell_init(20,100);
    coor_s * cell_1 = new_cell_init(30,100);
    coor_s * cell_2 = new_cell_init(40,100);
    coor_s * cell_3 = new_cell_init(50,100);
    coor_s * cell_4 = new_cell_init(60,100);

    pushFront( cells, cell_0 );
    pushFront( cells, cell_1 );
    pushFront( cells, cell_2 );
    pushFront( cells, cell_3 );
    pushFront( cells, cell_4 );
}

coor_s * new_cell_init( uint16_t x_new, uint16_t y_new )
{
    coor_s * new_cell = (coor_s*)kmalloc( sizeof(coor_s), GFP_KERNEL );
    if( NULL == new_cell )
        return NULL;

    new_cell->x = x_new;
    new_cell->y = y_new;
    return new_cell;
}

void snake_print_all( snake_s * Snake )
{
    uint16_t i;
    node_t * p_node = Snake->cells->head;
    coor_s * p_coor;

    for(i = 0; i < Snake->cells->size; ++i)
    {   
        p_coor = (coor_s*)p_node;
        p_coor = container_of_my(p_coor, coor_s, coor_node);
        lcd_fill_rectangle( p_coor->x, p_coor->y, SNAKE_W, SNAKE_H, Snake->color );
        p_node = p_node->next;
    }
}

/*
*
*
*
BUG FUNCTION
*
*
*
*/

void bug_rand( void )
{
    uint i;
    do
    {
        get_random_bytes(&i, 1);
        
        BUG.xy.x = (i % 32) * 10;  //0-31
        BUG.xy.y = (2 + i % 22) * 10;  //2-23
    } while ( bug_check() );
}

int bug_check( void )
{
    int i;
    node_t * p_node = mySnake.cells->head;
    coor_s * p_coor;

    for( i = 0; i < mySnake.cells->size; i++ )
    {
        p_coor = (coor_s*)p_node;
        p_coor = container_of_my(p_coor, coor_s, coor_node);
        //p_coor = container_of(p_node, coor_s, coor_node);
        if( p_coor->x == BUG.xy.x && p_coor->y == BUG.xy.y )
             return 1;
 
        p_node = p_node->next;
    }
    return 0;
}

void bug_print( void )
{
    lcd_fill_rectangle( BUG.xy.x + 3, BUG.xy.y + 3, 5, 5, COLOR_S_WHITE );
}




/*
*
*
*
LIST FUNCTION
*
*
*
*/
LinkedList *createLinkedList( void ) {
    LinkedList *new_list = (LinkedList*)kmalloc( sizeof(LinkedList), GFP_KERNEL );
    new_list->size = 0;
    new_list->head = new_list->tail = NULL;
    
    return new_list;
}

void pushFront( LinkedList * list, coor_s * coor_new ) 
{
    coor_new->coor_node.next = list->head;
    coor_new->coor_node.prev = NULL;

    if(list->head) {
        list->head->prev = &coor_new->coor_node;
    }
    list->head = &coor_new->coor_node;

    if(list->tail == NULL) {
        list->tail = &coor_new->coor_node;
    }
    list->size++;
}

void deleteTail( LinkedList * list )
{
    node_t * tail_free = list->tail;

    list->tail = list->tail->prev;
    list->tail->next = NULL;
    kfree(tail_free);
    list->size--;
}

void deleteLinkedList( LinkedList **list ) 
{
    node_t *tmp = (*list)->head;
    node_t *next = NULL;
    coor_s * p_coor;

    while(tmp) {
        next = tmp->next;
        p_coor = container_of(tmp, coor_s, coor_node);      
        kfree(p_coor);
        tmp = next;
    }
    kfree(*list);
    (*list) = NULL;
}



