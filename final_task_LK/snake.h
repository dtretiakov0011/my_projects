#ifndef _SNAKE_H_
#define _SNAKE_H_

#include "buttons.h"

#define SNAKE_SIZE  100
#define SNAKE_W     10
#define SNAKE_H     10

// Color definitions
#define COLOR_S_BLACK   0x0000
#define COLOR_S_BLUE    0x001F
#define COLOR_S_RED     0xF800
#define COLOR_S_GREEN   0x07E0
#define COLOR_S_CYAN    0x07FF
#define COLOR_S_MAGENTA 0xF81F
#define COLOR_S_YELLOW  0xFFE0
#define COLOR_S_WHITE   0xFFFF

/************list*********/
typedef struct node {
    struct node *next;
    struct node *prev;
} node_t;

typedef struct _LinkedList {
    uint16_t   size;
    node_t   * head;
    node_t   * tail;
}               LinkedList;
/*************************/

typedef struct coordinate
{
    struct node coor_node;
    uint16_t x;
    uint16_t y;
}              coor_s;

typedef struct snake
{
    LinkedList * cells;
    uint16_t color;
    uint16_t size;
    uint16_t score;
    uint16_t del_tail;
}              snake_s;




void snake_Init( snake_s * Snake );
void coor_xy_zero( coor_s * coor_xy, int size );
void coor_xy_start_init( snake_s * Snake );
void snake_print_all( snake_s * Snake );
void new_game( void );
void snake_clean_tail( coor_s * tail );
int check_priv_coor_XY( coor_s * head, coor_s * prev );
void snake_move_forward( coor_s * head, coor_s * tail );
void snake_move_left( coor_s * head, coor_s * tail );
void snake_move_right( coor_s * head, coor_s * tail );
void check_new_coor( coor_s * new_coor );
void direction_trigger( coor_s *head, coor_s *tail );
void play_game(snake_s * Snake);
void END_GAME( void );
void print_score( void );
int check_new_coor_with_snake( coor_s * new_coor );

/*********cells*********/
coor_s * new_cell_init( uint16_t x_new, uint16_t y_new );
void cells_start_init( LinkedList * cells );



/******************************************/
#define container_of_my(ptr, type, member) ((type *)((char *)(ptr) - offsetof(type, member)))

LinkedList * createLinkedList(void);
void pushFront( LinkedList * list, coor_s * coor_new );
void deleteLinkedList(LinkedList **list);
void deleteTail(LinkedList * list);
/**************************************/



/*****BUG******************/
typedef struct bug
{
    coor_s xy;
}              bug_s;



void bug_rand( void );
void bug_print( void );
int bug_check( void );
/**************************/



#endif
