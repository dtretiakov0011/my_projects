Task: write a Guess the Number game using the Bash script.

Subtask: create a random number generator script.

Task02: create script

script: Implement generates and outputs a random number in the range 0 to 5
Subtask completed.

Subtask: Extract Function refactoring.

Task02: Implement extract function rang_func
Subtask completed.

Subtask:  create a compare script.

Task02: implement compare script
Subtask completed.

Subtask: Prompt the user for a number if not specified by a command line parameter
Subtask completed.

Subtask: Prompt the user for a number limit of the range of random numbers by the second command line parameter as an integer from 1 to 100.

Subtask completed.

Subtask 12th: Prompt the user to enter an upper bound value if the second command line parameter is not specified.
Subtask completed.

Subtask 13th: Implement the ability to specify the number of guessing attempts by the third command line parameter.
Subtask completed.

Subtask 14th: Prompt the user to enter the number of guessing attempts if the third command line parameter is not specified.
Subtask completed.
