#include "decode.h"

#include <stdio.h>

static void decode_data(char *data) {
    int data_len = strlen(data);

    int char_c = 0; 
    char symbol[2];

    for(int i = 0; i < data_len; ++i) {
        char_c = my_atoi(data[i++]);
        symbol[0] = data[i];
        for(int y = 0; y < char_c; ++y) {
            write(STDOUT_FILENO, symbol, 1);
        }
    }
    write(STDOUT_FILENO, "\n", 1);
}

int my_atoi(char a) {
    return (int)a - '0';
}

int main() {
    char *data = read_stdin();
    decode_data(data);
    free(data);
}

