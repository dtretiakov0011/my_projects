#include "encode.h"

static void encode_func(char *data) {
    int data_len = strlen(data);               
    char encode_data[data_len * 2];
    char *p_en = data;

    int c_char = 1;
    char s_char = *data;

    for(int i = 1, u = 0; i <= data_len; i++) {
                    
        if(s_char == *(data + i)) 
            c_char++; 
        else {
            encode_data[u++] = c_char + '0';
            encode_data[u++] = s_char;
            encode_data[u] = '\0';
            s_char = *(data + i);
            c_char = 1;
            }
        }

        write_to_stdout(encode_data, strlen(encode_data));

        exit(0);
    }

int main() {
    char *data = read_stdin();
    encode_func(data);
    free(data);
}

