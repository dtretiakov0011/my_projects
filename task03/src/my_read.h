#ifndef _MY_READ_H_
#define _MY_READ_H_

#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>


#define BUFF_SIZE 1024

char *read_stdin(void);

#endif
