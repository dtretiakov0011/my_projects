#include "my_read.h"
#include "write.h"

char *read_stdin(void) { 
    char *new_data = (char*)malloc(sizeof(char));
    char buff[BUFF_SIZE + 1] = { 0 };
    int counter = 0, p_buff_size = 0;

    while( (counter = read(STDIN_FILENO, &buff, BUFF_SIZE)) ) {
        if(counter <= BUFF_SIZE) {
            buff[counter] = '\0';
            p_buff_size += counter;
            new_data = realloc(new_data, p_buff_size);
            strncpy(new_data + (p_buff_size - counter), buff, BUFF_SIZE);
        }
    }
    return new_data;
}

// static char *read_stdin(void) {
//     char *new_data = (char*)malloc(sizeof(char));

//     char buff[BUFF_SIZE];

//     int c = getc(stdin);
//     int counter = 0, p_buff_size = 0;
//     while(c != EOF) {
//         buff[counter] = (char)c;
//         c = getc(stdin);
//         counter++;
//         if(counter == BUFF_SIZE) {
//             p_buff_size += counter;
//             new_data = realloc(new_data, p_buff_size);
//             strncpy(new_data + (p_buff_size - counter), buff, BUFF_SIZE);
//             counter = 0;
//         }
//     }
//     if(p_buff_size < BUFF_SIZE) {
//         p_buff_size += counter;
//         new_data = realloc(new_data, p_buff_size);
//         strncpy(new_data, buff, p_buff_size);
//     }
//                 write(1, new_data, p_buff_size);
//                 write(1, "\n", 1);
//     return new_data;
// }

// void read_main(void) {
//     char *data = read_stdin();
//     write_to_stdout(data, strlen(data));
//     free(data);
   
// }

