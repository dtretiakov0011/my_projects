#include "write.h"

void write_to_stdout(char *data, int len) {
    write(STDOUT_FILENO, data, len);
    write(STDOUT_FILENO, "\n", 1);
} 
