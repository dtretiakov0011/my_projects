#ifndef __ILI9341_H__
#define __ILI9341_H__

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <string.h>
#include "fonts.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

#define GPIO_PIN_RESET	LCD_PIN_RESET
#define GPIO_PIN_DC		LCD_PIN_DC
#define GPIO_PIN_CS		LCD_PIN_CS
#define GPIO_DIR		"/sys/class/gpio"

#define BUF_SIZE		64

#define DATA_SIZE	1000

#define LCD_PIN_CS 7
#define LCD_PIN_RESET 27
#define LCD_PIN_DC 22

#define ILI9341_MADCTL_MY  0x80
#define ILI9341_MADCTL_MX  0x40
#define ILI9341_MADCTL_MV  0x20
#define ILI9341_MADCTL_ML  0x10
#define ILI9341_MADCTL_RGB 0x00
#define ILI9341_MADCTL_BGR 0x08
#define ILI9341_MADCTL_MH  0x04

// default orientation
#define LCD_WIDTH  320
#define LCD_HEIGHT 240
#define LCD_ROTATION (ILI9341_MADCTL_MX | ILI9341_MADCTL_BGR)

#define LCD_CASET 0x2A
#define LCD_RASET 0x2B
#define LCD_RAMWR 0x2C

/****************************/

// Color definitions
#define COLOR_BLACK   0x0000
#define COLOR_BLUE    0x001F
#define COLOR_RED     0xF800
#define COLOR_GREEN   0x07E0
#define COLOR_CYAN    0x07FF
#define COLOR_MAGENTA 0xF81F
#define COLOR_YELLOW  0xFFE0
#define COLOR_WHITE   0xFFFF
#define COLOR_COLOR565(r, g, b) (((r & 0xF8) << 8) | ((g & 0xFC) << 3) | ((b & 0xF8) >> 3))

int gpio_init(unsigned int gpio);
int gpio_free(unsigned int gpio);
int gpio_set_value(unsigned int gpio, unsigned int value);
void lcd_init_ili9341(void);
void lcd_set_address_window(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);
void lcd_update_screen(void);
void lcd_put_char(uint16_t x, uint16_t y, char ch, FontDef font, uint16_t color, uint16_t bgcolor);
void lcd_put_string(uint16_t x, uint16_t y, char *str, FontDef font, uint16_t color, uint16_t bgcolor);
void lcd_fill_screen(uint16_t color);






#endif // __ILI9341_H__


