#include "task04.h"
#include "ili9341.h"
#include "fonts.h"


void my_main(void) {

	lcd_init_ili9341();
	lcd_set_address_window(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);


	lcd_fill_screen(COLOR_GREEN);


    lcd_put_string(5, 210, "192.168.3.59", Font_11x18, COLOR_RED, COLOR_BLACK);
    //print_lcd_time();

	//lcd_update_screen();


//TEST
	test();


}



void *print_lcd_time() {
    long int ttime;
    char time_buf[26];
    
    time(&ttime);
    ctime_r(&ttime, time_buf);
    char tt[9] = { '0' };
    memcpy(tt, &time_buf[11], 8);
    lcd_put_string(210, 10, tt, Font_11x18, COLOR_RED, COLOR_BLACK);
}

void test() {
lcd_button_init();

int counter = 0;
char str_co[4];

int fd1, fd2, fd3;
char buf1[BUF_SIZE], buf2[BUF_SIZE], buf3[BUF_SIZE];
uint8_t value1 = 9, value2 = 9, value3 = 9;;

snprintf (buf1, sizeof(buf1), GPIO_DIR "/gpio%d/value", 18);
snprintf (buf2, sizeof(buf2), GPIO_DIR "/gpio%d/value", 23);
snprintf (buf3, sizeof(buf3), GPIO_DIR "/gpio%d/value", 24);



while(1) {
    print_lcd_time();
	fd1 = open (buf1, O_RDONLY);
	fd2 = open (buf2, O_RDONLY);
	fd3 = open (buf3, O_RDONLY);
	if (fd1 < 0 || fd3 < 0 || fd2 < 0) {
		perror ("gpio/get-value");
		//return fd;
	}
	read(fd1, &value1, 1);
	read(fd2, &value2, 1);
	read(fd3, &value3, 1);
	close(fd1);
	close(fd2);
	close(fd3);
	if(value2 == 48) {
		fd2 = open (buf2, O_RDONLY);
		read(fd2, &value2, 1);
		close(fd2);
		if(value2 == 48) { counter = 0;}
		sleep(2);
		fd2 = open (buf2, O_RDONLY);
		read(fd2, &value2, 1);
		close(fd2);
		if(value2 == 48) { break; }
	}
	if(value1 == 48) { counter++; };
	if(value3 == 48 && counter > 0) { counter--; };

	sprintf(str_co, "%d", counter);

	lcd_put_string(150, 110, str_co, Font_11x18, COLOR_RED, COLOR_BLACK );
	lcd_update_screen();
	usleep(130000);
}

lcd_button_free();
}



void lcd_button_init(void) {
    gpio_init(18);
    gpio_init(24);
    gpio_init(23);
    gpio_set_value(18, 1);
    gpio_set_value(24, 1);
    gpio_set_value(23, 1);
}

void lcd_button_free(void) {
    gpio_free(18);
    gpio_free(23);
    gpio_free(24);
}

