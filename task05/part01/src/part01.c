#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("denys");
MODULE_DESCRIPTION("task05");
MODULE_VERSION("0.1");

static void convert_to_from(void);
static void str_is_float( char* cof, int* zero_c, int* cof_i );

static char* cof;
module_param( cof, charp, 0);

static int uan = 100;
module_param( uan, int, 0);

static int eur = 3;
module_param( eur, int, 0);

static int __init task05_init(void)
{
    printk(KERN_INFO "init task05 !!!\n");
    convert_to_from();
    return 0;
}

static void convert_to_from(void) 
{
    int zero_c = 0, cof_i = 0, cof_int;

        str_is_float( cof, &zero_c, &cof_i );
        kstrtoint( &cof[cof_i], 0, &cof_int);

    if(cof[0] == '0')
    {
        int pow_ten = 10;
        int uan_to_eur, eur_to_uan;

        while(zero_c-- != 0)
            pow_ten *= 10;

        uan_to_eur = uan * cof_int / pow_ten;
        eur_to_uan = eur * cof_int;
        printk( "%d uan equal %d eur !\n", uan, uan_to_eur );
        printk( "%d eur equal %d uan !\n", eur, eur_to_uan );
    }
    else
    {
        int uan_to_eur = uan / cof_int;
        int eur_to_uan = eur * cof_int;
        printk( "%d uan equal %d eur !\n", uan, uan_to_eur );
        printk( "%d eur equal %d uan !\n", eur, eur_to_uan );
    }

}

static void str_is_float( char* cof, int* zero_c, int* cof_i )
{
    int i;

    for(i = 0; ;i++)
    {
        if( (cof[i] == '0') || (cof[i] == '.') )
        {
            if( cof[i] == '0' )
            (*zero_c) += 1;
        }
        else if( cof[i] == '.' )
            continue;
        else
            break;
    }
    *cof_i = i;
}

static void __exit task05_exit(void)
{
    printk(KERN_INFO "Bye...task05\n");
}

module_init(task05_init);
module_exit(task05_exit);


