#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <asm/uaccess.h>

//
#include "proc.h"
//

MODULE_LICENSE( "GPL" );
MODULE_AUTHOR( "Oleg Tsiliuric <olej@front.ru>" );
MODULE_VERSION( "6.3" );

//
    int USER_ID;
    void user_id( void );
    
    #define buf_size 125
  
    static char buffer[buf_size];

    static int user_id_check( int pos ) ;
//

static ssize_t dev_read( struct file * file, char * buf,
                           size_t count, loff_t *ppos ) {

    int len;
    int pos = 0, msg_pos = 0;
    static char mesg_buf[buf_size];

    user_id();

    while(buffer[pos] != '\0') 
    {
        if( user_id_check(pos) )
        {
            for( ; buffer[pos] != '\n'; ++pos, ++msg_pos ) 
            {
                mesg_buf[msg_pos] = buffer[pos];
            }
            mesg_buf[msg_pos++] = buffer[pos++];
        }
        else
        {
            while( buffer[pos] != '\n' ) { pos++; }
            
            pos++;
        }
    }
    //    printk( KERN_INFO "=== read : %ld\n", (long)count );
//    if( count < len ) return -EINVAL;
//    if( *ppos != 0 ) {
//       printk( KERN_INFO "=== read return : 0\n" );  // EOF
//       return 0;
//    }
//    if( copy_to_user( buf, hello_str, len ) ) return -EINVAL;
//    *ppos = len;
//    printk( KERN_INFO "=== read return : %d\n", len );


   if( *ppos != 0 ) {
      printk( KERN_INFO "=== read return : 0\n" );  // EOF
      return 0;
   }

    len = strlen( mesg_buf );
    if( copy_to_user(buf, mesg_buf, len) ) return -EINVAL;
    *ppos = len;

    return len;
}

static int user_id_check( int pos ) 
{
    int user_id;
    char str_user_id[3] = { '\0', '\n', '\0' };
    str_user_id[0] = buffer[pos];

    kstrtoint(str_user_id, 0, &user_id);

    if( user_id == USER_ID )
    {
        return 1;
    }
    else
    {
        return 0;
    }
}



static ssize_t dev_write( struct file * file, const char * buf,
                           size_t count, loff_t *ppos ) {

    char f_u_buff[buf_size];

    copy_from_user(f_u_buff, buf, count);
    f_u_buff[count - 1] = '\0';

    strcat(buffer, f_u_buff);
    strcat(buffer, "\n");

   return count;
}


//
void user_id( void )
{
   char my_proc_buf[BUFFER_SIZE];

   strcpy(my_proc_buf, proc_buffer);
   kstrtoint(my_proc_buf, 0, &USER_ID);
}
//



static int __init dev_init( void );
module_init( dev_init );

static void __exit dev_exit( void );
module_exit( dev_exit );

