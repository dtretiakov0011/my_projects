#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/jiffies.h>
#include <linux/time.h>


MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Aleksandr Bulyshchenko <A.Bulyshchenko@globallogic.com>");
MODULE_DESCRIPTION("Example for procfs read/write");
MODULE_VERSION("0.1");


#define MODULE_TAG      "example_module "
#define PROC_DIRECTORY  "reminder"
#define PROC_FILENAME   "buffer"
#define PROC_FILENAME_MY   "seconds"
#define BUFFER_SIZE     10

//
static u32 absolute;
static u32 j;
static u32 j1;

typedef struct Reminder
{
    char *  user_name;
    char *  message;
    int     seconds;
}              Reminder_s;

static int counter_s;

static Reminder_s reminder_global[10];

static void add_to_reminder(char *str_buff);
static void add_to_reminder(char *str_buff);
static void cpy_str_from_user( char *dest, const char *str, int len );
static void clean_reminder( void );
//

static char *proc_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;
static struct proc_dir_entry *proc_file_timer;

static ssize_t my_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);
static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);
//proc_ops
//file_operations
static struct proc_ops proc_fops = {
    .proc_read  = example_read,
    .proc_write = example_write,
};

static struct proc_ops my_proc_fops = {
    .proc_write = my_write,
};


static int create_buffer(void)
{
    proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_buffer)
        return -ENOMEM;
    proc_msg_length = 0;

    return 0;
}


static void cleanup_buffer(void)
{
    if (proc_buffer) {
        kfree(proc_buffer);
        proc_buffer = NULL;
    }
    proc_msg_length = 0;
}


static int create_proc_example(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops);
    if (NULL == proc_file)
        return -EFAULT;

    proc_file_timer = proc_create(PROC_FILENAME_MY, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &my_proc_fops);
    if (NULL == proc_file_timer)
        return -EFAULT;

    return 0;
}


static void cleanup_proc_example(void)
{
    if (proc_file)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file = NULL;
    }
    if (proc_file_timer)
    {
        remove_proc_entry(PROC_FILENAME_MY, proc_dir);
        proc_file_timer = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}


static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
    size_t left;

    if (length > (proc_msg_length - proc_msg_read_pos))
        length = (proc_msg_length - proc_msg_read_pos);

    left = copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);

    proc_msg_read_pos += length - left;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to read %u from %u chars\n", left, length);
    else
    {
        u32 j_result;
        time64_t totalsecs; 
        struct tm result;

        j1 = jiffies;
        j_result = j1 - j;
        j = jiffies;

        if( j_result / HZ != 0 )
        {
            totalsecs = j_result / HZ;
            time64_to_tm( totalsecs, 0, &result );

            if( proc_buffer[0] == '1' )
                printk( KERN_NOTICE MODULE_TAG "seconds from last read = %d(sec) \n", j_result / HZ );
            else if( proc_buffer[0] == '2' )
                printk( KERN_NOTICE MODULE_TAG "full time = %02d:%02d:%02d \n", result.tm_hour, result.tm_min, result.tm_sec );
            // else if( proc_buffer[0] == '3' )
            // {
            //     struct timespec64 time;
            //     struct tm result1;

            //     ktime_get_ts64(&time);
            //     time64_to_tm( time.tv_sec, 0, &result1 );
            //     printk( KERN_NOTICE MODULE_TAG "absolute time = %d:%d:%d \n", result1.tm_hour, result1.tm_min, result1.tm_sec );

            //     // totalsecs = absolute; 
            //     // time64_to_tm( totalsecs, 0, &result );  
            //     // printk( KERN_NOTICE MODULE_TAG "absolute time = %d:%d:%d \n", result.tm_hour, result.tm_min, result.tm_sec );
            // }
                
        }

        //test
        // {
        //     struct timespec64 time;
        //     struct tm result1;

        //     ktime_get_ts64(&time);
        //     time64_to_tm( time.tv_sec, 0, &result1 );
        //     printk( KERN_NOTICE MODULE_TAG "absolute time = %d:%d:%d \n", result1.tm_hour, result1.tm_min, result1.tm_sec );

        // }


        
    }

    return length - left;
}


static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
    size_t msg_length;
    size_t left;

    if (length > BUFFER_SIZE)
    {
        printk(KERN_WARNING MODULE_TAG "reduse message length from %u to %u chars\n", length, BUFFER_SIZE);
        msg_length = BUFFER_SIZE;
    }
    else
        msg_length = length;

    //

//
    char buf[256] = { 0 };
    
    cpy_str_from_user( buf, buffer, length );

    add_to_reminder(buf);
//
    left = copy_from_user(proc_buffer, buffer, msg_length);
    proc_msg_length = msg_length - left;
    proc_msg_read_pos = 0;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write %u from %u chars\n", left, msg_length);
    else
        printk(KERN_NOTICE MODULE_TAG "written %u chars\n", msg_length);

    return length;
}

static ssize_t my_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
    char timer_buf[5] = {0};
    int seconds;

    cpy_str_from_user( timer_buf, buffer, length );
    timer_buf[ length - 1 ] = '\n';
    timer_buf[ length ] = '\0';

    kstrtoint( timer_buf, 0, &seconds );

    printk( "*********%d***%s|****\n", seconds, timer_buf );

    return length;
}


static int __init example_init(void)
{
    int err;

    j = jiffies;
    absolute = jiffies;

    err = create_buffer();
    if (err)
        goto error;

    err = create_proc_example();
    if (err)
        goto error;

    printk(KERN_NOTICE MODULE_TAG "loaded\n");
    return 0;

error:
    printk(KERN_ERR MODULE_TAG "failed to load\n");
    cleanup_proc_example();
    cleanup_buffer();
    return err;
}


static void __exit example_exit(void)
{
    clean_reminder();
    cleanup_proc_example();
    cleanup_buffer();
    printk(KERN_NOTICE MODULE_TAG "exited\n");
}



static void cpy_str_from_user( char *dest, const char *str, int len ) 
{
    strncpy( dest, str, len );
    dest[ len - 1 ] = '\0';
}

static void add_to_reminder(char *str_buff)
{
    int i = 0, y;


    while ( str_buff[i] != ' ' )
        i++;
    
    reminder_global[counter_s].user_name = kcalloc( 1, i, GFP_KERNEL );
    strncpy( reminder_global[counter_s].user_name, str_buff, i );

    i++; y = i;

    while( str_buff[y] != '\0' )
        y++;

    int msg_size = y - i + 1;

    reminder_global[counter_s].message = kcalloc( 1, msg_size, GFP_KERNEL );
    strncpy( reminder_global[counter_s].message, &str_buff[i], msg_size );

//test
    printk( "name=|%s|\n", reminder_global[counter_s].user_name );
    printk( "MSG=|%s|\n", reminder_global[counter_s].message );
//
    counter_s++;
}

static void clean_reminder( void )
{
    int i;
    for( i = counter_s; i >= 0; i-- )
    {
        kfree(reminder_global[i].user_name);
        kfree(reminder_global[i].message);
    }
}


module_init(example_init);
module_exit(example_exit);
